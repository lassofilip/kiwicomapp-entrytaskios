//
//  LocationManager.swift
//  kiwicomapp-entrytaskios
//
//  Created by Filip Laššo on 18/01/2021.
//  Copyright © 2021 Kiwi. All rights reserved.
//

import CoreLocation

final class LocationManager: NSObject, CLLocationManagerDelegate {

    // MARK: - Properties
    
    private let locationManager: CLLocationManager
    var onAuthorizationDetermined: (() -> Void)?

    // MARK: - Lifecycle
    
    override init() {
        locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        super.init()
        locationManager.delegate = self
    }
    
    // MARK: - Helpers
    
    func coordinates() -> (lat: CLLocationDegrees, lon: CLLocationDegrees)? {
        guard
            let lat = locationManager.location?.coordinate.latitude,
            let lon = locationManager.location?.coordinate.longitude
            else { return nil }
        return (lat, lon)
    }
    
    // MARK: - CLLocationManagerDelegate
    
    func locationManager(
        _ manager: CLLocationManager,
        didChangeAuthorization status: CLAuthorizationStatus
    ) {
        if status == .notDetermined {
            manager.requestWhenInUseAuthorization()
            return
        } else if status == .authorizedWhenInUse {
            manager.startUpdatingLocation()
        }
        onAuthorizationDetermined?()
    }
}
