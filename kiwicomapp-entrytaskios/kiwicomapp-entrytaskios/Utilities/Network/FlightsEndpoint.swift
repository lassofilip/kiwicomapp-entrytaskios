//
//  FlightsEndpoint.swift
//  kiwicomapp-entrytaskios
//
//  Created by Filip Laššo on 19/01/2021.
//  Copyright © 2021 Kiwi. All rights reserved.
//

enum FlightsEndpoint: Endpoint {
    
    // MARK: - Cases
    
    case flightsToUniqueDestinations(dateFrom: String, flyFrom: String, limit: String)
    
    // MARK: - Properties
    
    var path: String {
        return "flights"
    }
    var queryParameters: [String : String]? {
        var defaultQueryParameters = [
            "fly_from": NetworkConstants.flyFrom,
            "limit": NetworkConstants.limit,
            "locale": NetworkConstants.locale,
            "partner": NetworkConstants.partner,
            "v": NetworkConstants.version
        ]
        switch self {
        case .flightsToUniqueDestinations(let dateFrom, let flyFrom, let limit):
            defaultQueryParameters["adults"] = "1"
            defaultQueryParameters["flight_type"] = "oneway"
            defaultQueryParameters["fly_from"] = flyFrom
            defaultQueryParameters["fly_to"] = "anywhere"
            defaultQueryParameters["dateFrom"] = dateFrom
            defaultQueryParameters["limit"] = limit
            defaultQueryParameters["one_for_city"] = "1"
            return defaultQueryParameters
        }
    }
}
