//
//  Endpoint.swift
//  kiwicomapp-entrytaskios
//
//  Created by Filip Laššo on 19/01/2021.
//  Copyright © 2021 Kiwi. All rights reserved.
//

import Foundation

protocol Endpoint {
    
    var path: String { get }
    var queryParameters: [String : String]? { get }
}
