//
//  NetworkConstants.swift
//  kiwicomapp-entrytaskios
//
//  Created by Filip Laššo on 19/01/2021.
//  Copyright © 2021 Kiwi. All rights reserved.
//

struct NetworkConstants {
    
    /// Limit number of results.
    static let limit = "10"
    
    /// Departure location.
    static let flyFrom = "LON"
    
    /// Radius for departure location.
    static let radius = "250km"
    
    /// Language of city names in the response.
    static let locale = "en"
    
    /// Partner ID.
    static let partner = "picky"
    
    /// Geographical data API version.
    static let version = "3"
    
    /// Partial URL path to city image.
    ///
    /// ⚠️ Append `{name}.jpg` to complete full URL path.
    static let imagePartialUrlPath = "https://images.kiwi.com/photos/600x330/"
}
