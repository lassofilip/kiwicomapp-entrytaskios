//
//  NetworkManager.swift
//  kiwicomapp-entrytaskios
//
//  Created by Filip Laššo on 18/01/2021.
//  Copyright © 2021 Kiwi. All rights reserved.
//

import Foundation

final class NetworkManager {
    
    // MARK: - Properties
    
    let urlSession: URLSession
    
    // MARK: - Lifecycle
    
    init(urlSession: URLSession) {
        self.urlSession = urlSession
    }
    
    // MARK: - Requests
    
    func request<T: Codable>(
        with url: URL,
        completion: @escaping (Result<T, Error>) -> ()
    ) {
        urlSession.dataTask(with: url) { data, response, error in
            if let error = error {
                completion(.failure(error))
                return
            }
            guard
                response != nil,
                let data = data
                else { return }
            DispatchQueue.main.async {
                do {
                    let responseObject = try JSONDecoder().decode(T.self, from: data)
                    completion(.success(responseObject))
                } catch let error {
                    completion(.failure(error))
                }
            }
        }.resume()
    }
}
