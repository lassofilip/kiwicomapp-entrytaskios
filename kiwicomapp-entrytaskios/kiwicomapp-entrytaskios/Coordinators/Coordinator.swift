//
//  Coordinator.swift
//  kiwicomapp-entrytaskios
//
//  Created by Filip Laššo on 20/01/2021.
//  Copyright © 2021 Kiwi. All rights reserved.
//

import UIKit

class Coordinator: NSObject {
    
    // MARK: - Properties
    
    public private(set) var children = [Coordinator]()
    public lazy var rootControllerMovingFromParent: () -> Void = { [weak self] in self?.finish() }
    /// Automatically send value in `finish` and subscribe in `start`
    private var onEnd: (Coordinator) -> Void = { _ in }
    
    // MARK: - Lifecycle
    
    /// Perform initial navigation.
    /// ⚠️ This method should be overriden.
    func start() {
        preconditionFailure("This method should be overriden by every coordinator")
    }
    
    /// Add coordinator to children, set it`s parent to self, setup finist ovservation and start flow.
    /// - parameters:
    ///     - coordinator: Coordinator which will be added to children.
    func startChildCoordinator(_ coordinator: Coordinator) {
        children.append(coordinator)
        coordinator.start()
        coordinator.onEnd = { [weak self] in
            self?.removeChild($0)
        }
    }
    
    private func removeChild(_ child: Coordinator) {
        children = children.filter { $0 !== child }
    }
    
    /// Notifies parent coordinator about removal.
    /// ⚠️ When overriding this method, make sure you call `super.finish` otherwise coordinator won`t send message to parent.
    func finish() {
        onEnd(self)
    }
}
