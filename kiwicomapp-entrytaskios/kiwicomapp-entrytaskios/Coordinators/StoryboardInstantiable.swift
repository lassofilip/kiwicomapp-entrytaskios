//
//  StoryboardInstantiable.swift
//  kiwicomapp-entrytaskios
//
//  Created by Filip Laššo on 20/01/2021.
//  Copyright © 2021 Kiwi. All rights reserved.
//

import UIKit

protocol StoryboardInstantiable: NSObjectProtocol {
    
    associatedtype T
    static var fileName: String { get }
    static func instantiateViewController(_ bundle: Bundle?) -> T
}

/// To use this extension, you need to create a separate storyboard for each UIViewController. The name of the storyboard must match the name of the UIViewController‘s class. This UIViewController must be set as the initial UIViewController for this storyboard.
extension StoryboardInstantiable where Self: UIViewController {
    
    static var fileName: String {
        return NSStringFromClass(Self.self).components(separatedBy: ".").last!
    }

    static func instantiateViewController(_ bundle: Bundle? = nil) -> Self {
        let storyboard = UIStoryboard(name: fileName, bundle: bundle)
        return storyboard.instantiateInitialViewController() as! Self
    }
}

extension UIViewController: StoryboardInstantiable {}
