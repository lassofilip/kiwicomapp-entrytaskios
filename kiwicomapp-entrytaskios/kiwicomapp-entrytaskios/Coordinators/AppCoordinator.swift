//
//  AppCoordinator.swift
//  kiwicomapp-entrytaskios
//
//  Created by Filip Laššo on 20/01/2021.
//  Copyright © 2021 Kiwi. All rights reserved.
//

import Foundation
import UIKit

final class AppCoordinator: Coordinator {
    
    // MARK: - Properties
    
    private let window: UIWindow

    // MARK: - Lifecycle
    
    init(in window: UIWindow) {
        self.window = window
    }
    
    override func start() {
        let controller = FlightOffersViewController.instantiateViewController()
        let navigationController = UINavigationController(rootViewController: controller)
        window.rootViewController = navigationController
        window.makeKeyAndVisible()
    }
}
