//
//  FlightOffersCollectionViewCell.swift
//  kiwicomapp-entrytaskios
//
//  Created by Filip Laššo on 16/01/2021.
//  Copyright © 2021 Kiwi. All rights reserved.
//

import UIKit

final class FlightOffersCollectionViewCell: UICollectionViewCell,
    UIScrollViewDelegate {

    @IBOutlet private weak var scrollView: UIScrollView!
    @IBOutlet private weak var destinationImageView: UIImageView!
    @IBOutlet private weak var destinationImageViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet private weak var destinationLabel: UILabel!
    @IBOutlet private weak var priceLabel: UILabel!
    @IBOutlet private weak var departureLabel: UILabel!
    @IBOutlet private weak var departureDateLabel: UILabel!
    @IBOutlet private weak var flightDurationLabel: UILabel!
    @IBOutlet private weak var distanceLabel: UILabel!
    var flight: Flight? {
        didSet {
            setupUI()
        }
    }
    var scrollViewContentOffsetY: CGFloat? {
        get {
            return scrollView.contentOffset.y
        }
        set {
            scrollView.setContentOffset(CGPoint(x: 0, y: newValue ?? 0), animated: false)
        }
    }
    
    // MARK: - UIScrollViewDelegate
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offset = scrollView.contentOffset
        if offset.y < 0.0 {
            var transform = CATransform3DTranslate(CATransform3DIdentity, 0, offset.y, 0)
            let scaleFactor = 1 + (-1 * offset.y / (destinationImageViewHeightConstraint.constant / 2))
            transform = CATransform3DScale(transform, scaleFactor, scaleFactor, 1)
            destinationImageView.layer.transform = transform
        } else {
            destinationImageView.layer.transform = CATransform3DIdentity
        }
    }
    
    // MARK: - Helpers
    
    private func setupUI() {
        guard let flight = flight else { return }
        destinationLabel.text = flight.cityTo
        priceLabel.text = "\(flight.conversion.eur) EUR"
        departureLabel.attributedText = "Departure: \(flight.cityFrom), \(flight.flyFrom)".withBoldText(text: "Departure:")
        let date = Date(timeIntervalSince1970: flight.dTime)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd.MM.yyyy HH:mm"
        let dateStr = dateFormatter.string(from: date)
        departureDateLabel.attributedText = "Departure date: \(dateStr)".withBoldText(text: "Departure date:")
        flightDurationLabel.attributedText = "Flight duration: \(flight.flyDuration)".withBoldText(text: "Flight duration:")
        distanceLabel.attributedText = "Distance: \(flight.distance)km".withBoldText(text: "Distance:")
        let urlString = "\(NetworkConstants.imagePartialUrlPath + flight.mapIdTo).jpg"
        destinationImageView.loadImageAsync(with: urlString)
    }
}
