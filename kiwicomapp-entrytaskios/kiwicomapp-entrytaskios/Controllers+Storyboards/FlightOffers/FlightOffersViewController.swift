//
//  FlightOffersViewController.swift
//  kiwicomapp-entrytaskios
//
//  Created by Filip Laššo on 15/01/2021.
//  Copyright © 2021 Kiwi. All rights reserved.
//

import UIKit
import CoreLocation

final class FlightOffersViewController: UIViewController,
    UICollectionViewDelegate,
    UICollectionViewDataSource,
    UICollectionViewDelegateFlowLayout {

    // MARK: - Properties
    
    @IBOutlet private weak var collectionView: UICollectionView!
    @IBOutlet private weak var pageControl: UIPageControl!
    @IBOutlet private weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet private weak var errorLabel: UILabel!
    private var currentPage = 0 {
        didSet {
            pageControl.currentPage = currentPage
        }
    }
    private var flights = [Flight]() {
        didSet {
            collectionView.reloadData()
            pageControl.numberOfPages = flights.count
            pageControl.currentPage = currentPage
        }
    }
    private var cellsContentOffsets: [Int: CGFloat] = [:]
    private lazy var locationManager: LocationManager = {
        LocationManager()
    }()
    private lazy var networkManager: NetworkManager = {
        let urlSession = URLSession(configuration: .default)
        let networkManager = NetworkManager(urlSession: urlSession)
        return networkManager
    }()
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.register(UINib(nibName: "FlightOffersCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "Cell")
        if CLLocationManager.locationServicesEnabled() {
            locationManager.onAuthorizationDetermined = { [weak self] in
                self?.fetchFlights()
            }
        } else {
            fetchFlights()
        }
    }
    
    // MARK: - Data loading
    
    private func fetchFlights() {
        setupLoadingUI()
        let nextFetch = UserDefaults.standard.object(forKey: UserDefaults.Keys.nextDownload) as? Date ?? Date()
        if
            nextFetch > Date(),
            let cachedFlightOffers = UserDefaults.standard.structArrayData(Flight.self, forKey: UserDefaults.Keys.activeFlightOffers) {
                activityIndicator.stopAnimating()
                flights = cachedFlightOffers
                return
        }
        downloadFlights()
    }
    
    private func downloadFlights() {
        setupLoadingUI()
        let dateFrom = Date().addDays(1).toString()
        var flyFrom = UserDefaults.standard.string(forKey: UserDefaults.Keys.flyFrom) ?? NetworkConstants.flyFrom
        if CLLocationManager.locationServicesEnabled() {
            if let coordinates = locationManager.coordinates() {
                flyFrom = "\(Float(coordinates.lat))-\(Float(coordinates.lon))-\(NetworkConstants.radius)"
                UserDefaults.standard.set(flyFrom, forKey: UserDefaults.Keys.flyFrom)
            }
        }
        let url = FlightsEndpoint.flightsToUniqueDestinations(dateFrom: dateFrom, flyFrom: flyFrom, limit: NetworkConstants.limit).url
        networkManager.request(with: url) { [weak self] (result: Result<FlightsResponse, Error>) in
            guard let self = self else { return }
            self.activityIndicator.stopAnimating()
            switch result {
            case .success(let flightsResponse):
                if let activeFlights = UserDefaults.standard.structArrayData(Flight.self, forKey: UserDefaults.Keys.activeFlightOffers) {
                    let uniqueFlights = flightsResponse.data.getUniqueFlights(according: activeFlights)
                    self.flights = Array(uniqueFlights.prefix(5))
                } else {
                    self.flights = Array(flightsResponse.data.prefix(5))
                }
                UserDefaults.standard.setStructArray(self.flights, forKey: UserDefaults.Keys.activeFlightOffers)
                UserDefaults.standard.set(Date().addDays(1), forKey: UserDefaults.Keys.nextDownload)
            case .failure(let error):
                print(error)
                self.errorLabel.isHidden = false
            }
        }
    }

    // MARK: - UICollectionViewDelegate
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        currentPage = Int(pageNumber)
    }
    
    func collectionView(
        _ collectionView: UICollectionView,
        didEndDisplaying cell: UICollectionViewCell,
        forItemAt indexPath: IndexPath
    ) {
        guard let cell = cell as? FlightOffersCollectionViewCell else { return }
        cellsContentOffsets[indexPath.row] = cell.scrollViewContentOffsetY
    }
    
    func collectionView(
        _ collectionView: UICollectionView,
        willDisplay cell: UICollectionViewCell,
        forItemAt indexPath: IndexPath
    ) {
        guard let cell = cell as? FlightOffersCollectionViewCell else { return }
        cell.scrollViewContentOffsetY = cellsContentOffsets[indexPath.row]
    }

    // MARK: - UICollectionViewDataSource
    
    func collectionView(
        _ collectionView: UICollectionView,
        numberOfItemsInSection section: Int
    ) -> Int {
        return flights.count
    }
    
    func collectionView(
        _ collectionView: UICollectionView,
        cellForItemAt indexPath: IndexPath
    ) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! FlightOffersCollectionViewCell
        cell.flight = flights[indexPath.row]
        return cell
    }
    
    // MARK: - UICollectionViewDelegateFlowLayout
    
    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAt indexPath: IndexPath
    ) -> CGSize {
        return collectionView.frame.size
    }
    
    // MARK: - Helpers
    
    private func setupLoadingUI() {
        flights.removeAll()
        errorLabel.isHidden = true
        activityIndicator.isHidden = false
        activityIndicator.startAnimating()
    }
}

private extension Array where Element == Flight {
    
    func getUniqueFlights(according activeFlights: [Flight]) -> [Flight] {
        filter { !activeFlights.containtsFlight($0) }
    }
    
    func containtsFlight(_ flight: Flight) -> Bool {
        contains(where: { $0.cityCodeTo == flight.cityCodeTo })
    }
}
