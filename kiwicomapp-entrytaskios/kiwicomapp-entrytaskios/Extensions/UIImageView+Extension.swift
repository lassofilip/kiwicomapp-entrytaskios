//
//  UIImageView+Extension.swift
//  kiwicomapp-entrytaskios
//
//  Created by Filip Laššo on 16/01/2021.
//  Copyright © 2021 Kiwi. All rights reserved.
//

import UIKit

extension UIImageView {
    
    // MARK: - Properites
    
    private static var taskKey = 0
    private static var urlKey = 0
    private var currentTask: URLSessionTask? {
        get { return objc_getAssociatedObject(self, &UIImageView.taskKey) as? URLSessionTask }
        set { objc_setAssociatedObject(self, &UIImageView.taskKey, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC) }
    }
    private var currentURL: URL? {
        get { return objc_getAssociatedObject(self, &UIImageView.urlKey) as? URL }
        set { objc_setAssociatedObject(self, &UIImageView.urlKey, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC) }
    }

    // MARK: - Loading
    
    func loadImageAsync(with urlString: String?) {
        weak var oldTask = currentTask
        currentTask = nil
        oldTask?.cancel()
        self.image = UIImage(named: "placeholder")
        guard let urlString = urlString else { return }
        let imageName = String(urlString.split(separator: "/").last!)
        if let cachedImage = image(name: imageName) {
            self.transition(toImage: cachedImage)
            return
        }
        let url = URL(string: urlString)!
        currentURL = url
        let task = URLSession.shared.dataTask(with: url) { [weak self] data, response, error in
            guard let self = self else { return }
            self.currentTask = nil
            if let unwrappedError = error {
                if (unwrappedError as NSError).domain == NSURLErrorDomain && (unwrappedError as NSError).code == NSURLErrorCancelled {
                    return
                }
                print(unwrappedError)
                return
            }
            guard
                let unwrappedData = data,
                let downloadedImage = UIImage(data: unwrappedData)
                else { return }
            self.cacheImage(name: imageName, image: downloadedImage)
            if url == self.currentURL {
                DispatchQueue.main.async {
                    self.transition(toImage: downloadedImage)
                }
            }
        }
        currentTask = task
        task.resume()
    }
    
    // MARK: - Caching
    
    private func cacheImage(name: String, image: UIImage) {
        guard let data = image.jpegData(compressionQuality: 1) ?? image.pngData() else { return }
        let cacheDirectoryURL = FileManager.default.cacheDirectory()
        let imageURL = cacheDirectoryURL.appendingPathComponent(name)
        do {
            try data.write(to: imageURL)
        } catch {
            print(error)
        }
    }
    
    private func image(name: String) -> UIImage? {
        let cacheDirectoryURL = FileManager.default.cacheDirectory()
        let imagePath = cacheDirectoryURL.appendingPathComponent(name).path
        if FileManager.default.fileExists(atPath: imagePath) {
            return UIImage(contentsOfFile: imagePath)
        }
        return nil
    }
    
    // MARK: - Helpers
    
    private func transition(toImage image: UIImage) {
        UIView.transition(
            with: self,
            duration: 0.3,
            options: [.transitionCrossDissolve],
            animations: { self.image = image }
        )
    }
}
