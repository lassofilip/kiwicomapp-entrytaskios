//
//  Bundle+Extension.swift
//  kiwicomapp-entrytaskios
//
//  Created by Filip Laššo on 16/01/2021.
//  Copyright © 2021 Kiwi. All rights reserved.
//

import Foundation

extension Bundle {

    var appName: String? {
        return object(forInfoDictionaryKey: "CFBundleDisplayName") as? String ?? object(forInfoDictionaryKey: "CFBundleName") as? String
    }
}
