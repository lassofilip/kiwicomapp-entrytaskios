//
//  String+Extension.swift
//  kiwicomapp-entrytaskios
//
//  Created by Filip Laššo on 20/01/2021.
//  Copyright © 2021 Kiwi. All rights reserved.
//

import UIKit

extension String {
    
    func withBoldText(text: String, font: UIFont? = nil) -> NSAttributedString {
        let font = font ?? UIFont.preferredFont(forTextStyle: .body)
        let fullString = NSMutableAttributedString(string: self, attributes: [NSAttributedString.Key.font: font])
        let boldFontAttribute: [NSAttributedString.Key: Any] = [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: font.pointSize)]
        let range = (self as NSString).range(of: text)
        fullString.addAttributes(boldFontAttribute, range: range)
        return fullString
    }
}
