//
//  Endpoint+Extension.swift
//  kiwicomapp-entrytaskios
//
//  Created by Filip Laššo on 19/01/2021.
//  Copyright © 2021 Kiwi. All rights reserved.
//

import Foundation

extension Endpoint {
    
    var scheme: String {
        return "https"
    }
    var host: String {
        return "api.skypicker.com"
    }
    var url: URL {
        var components = URLComponents()
        components.scheme = scheme
        components.host = host
        components.path = "/\(path)"
        if let queryParameters = queryParameters {
            components.queryItems = queryParameters.map { URLQueryItem(name: $0.key, value: $0.value) }
        }
        return components.url!
    }
}
