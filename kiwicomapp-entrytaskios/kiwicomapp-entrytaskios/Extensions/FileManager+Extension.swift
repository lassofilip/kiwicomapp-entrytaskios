//
//  FileManager+Extension.swift
//  kiwicomapp-entrytaskios
//
//  Created by Filip Laššo on 17/01/2021.
//  Copyright © 2021 Kiwi. All rights reserved.
//

import Foundation

extension FileManager {
    
    /// Get URL of cache directory. Also creates new cache directory If no cache directory exists.
    /// - returns: URL of cache directory.
    func cacheDirectory() -> URL  {
        let cacheDirectoryURL = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask)[0].appendingPathComponent(Bundle.main.appName!)
        let cacheDirectoryPath = cacheDirectoryURL.path
        if !FileManager.default.fileExists(atPath: cacheDirectoryPath) {
            createCacheDirectory(path: cacheDirectoryPath)
        }
        return cacheDirectoryURL
    }
    
    /// Creates cache directory at specified path.
    /// - parameters:
    ///     - path: Path to cache.
    func createCacheDirectory(path: String) {
        do {
            try FileManager.default.createDirectory(atPath: path, withIntermediateDirectories: true, attributes: nil)
        } catch {
            print(error)
        }
    }
}
