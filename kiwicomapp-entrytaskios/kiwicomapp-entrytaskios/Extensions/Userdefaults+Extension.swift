//
//  Userdefaults+Extension.swift
//  kiwicomapp-entrytaskios
//
//  Created by Filip Laššo on 18/01/2021.
//  Copyright © 2021 Kiwi. All rights reserved.
//

import Foundation

extension UserDefaults {
    
    enum Keys {
        static let activeFlightOffers = "activeFlightOffers"
        static let nextDownload = "nextDownload"
        static let flyFrom = "flyFrom"
    }
    
    open func setStruct<T: Codable>(_ value: T?, forKey defaultName: String){
        let data = try? JSONEncoder().encode(value)
        set(data, forKey: defaultName)
    }
    
    open func structData<T>(_ type: T.Type, forKey defaultName: String) -> T? where T : Decodable {
        guard let encodedData = data(forKey: defaultName) else {
            return nil
        }
        return try! JSONDecoder().decode(type, from: encodedData)
    }
    
    open func setStructArray<T: Codable>(_ value: [T], forKey defaultName: String){
        let data = value.map { try? JSONEncoder().encode($0) }
        set(data, forKey: defaultName)
    }
    
    open func structArrayData<T>(_ type: T.Type, forKey defaultName: String) -> [T]? where T : Decodable {
        guard let encodedData = array(forKey: defaultName) as? [Data] else {
            return nil
        }
        return encodedData.map { try! JSONDecoder().decode(type, from: $0) }
    }
}
