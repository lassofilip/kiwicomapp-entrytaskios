//
//  Date+Extension.swift
//  kiwicomapp-entrytaskios
//
//  Created by Filip Laššo on 19/01/2021.
//  Copyright © 2021 Kiwi. All rights reserved.
//

import Foundation

extension Date {
    
    func toString(format: String = "dd/MM/yyyy") -> String {
        let formatter = DateFormatter()
        formatter.dateStyle = .short
        formatter.dateFormat = format
        return formatter.string(from: self)
    }
    
    func addDays(_ days: Int) -> Date {
        let newDate = Calendar.current.date(byAdding: .day, value: days, to: self)
        return newDate ?? Date()
    }
}
