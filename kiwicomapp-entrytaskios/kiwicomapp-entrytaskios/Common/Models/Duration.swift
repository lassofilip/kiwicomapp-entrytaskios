//
//  Duration.swift
//  kiwicomapp-entrytaskios
//
//  Created by Filip Laššo on 16/01/2021.
//  Copyright © 2021 Kiwi. All rights reserved.
//

public struct Duration: Codable {
    
    let departure: Int
    let durationReturn: Int
    let total: Int
    
    enum CodingKeys: String, CodingKey {
        case departure, total
        case durationReturn = "return"
    }
}
