//
//  Country.swift
//  kiwicomapp-entrytaskios
//
//  Created by Filip Laššo on 15/01/2021.
//  Copyright © 2021 Kiwi. All rights reserved.
//

public struct Country: Codable {
    
    let code: String
    let name: String
}
