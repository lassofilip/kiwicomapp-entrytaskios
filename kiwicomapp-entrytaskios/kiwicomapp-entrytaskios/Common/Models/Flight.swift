//
//  Flight.swift
//  kiwicomapp-entrytaskios
//
//  Created by Filip Laššo on 16/01/2021.
//  Copyright © 2021 Kiwi. All rights reserved.
//
import Foundation

public struct Flight: Codable {
    
    let id: String
    let dTime: Double
    let dTimeUTC: Double
    let aTime: Double
    let aTimeUTC: Double
    let nightsInDest: Int?
    let duration: Duration
    let flyDuration: String
    let flyFrom: String
    let cityFrom: String
    let cityCodeFrom: String
    let countryFrom: Country
    let mapIdFrom: String?
    let flyTo: String
    let cityTo: String
    let cityCodeTo: String
    let countryTo: Country
    let mapIdTo: String
    let distance: Double
    let routes: [[String]]
    let airlines: [String]
    let pnrCount: Int
    let hasAirportChange: Bool?
    let technicalStops: Int
    let price: Int
    let bagsPrice: BagsPrice
    let baglimit: Baglimit
    let availability: Avaibility?
    let facilitatedBookingAvailable: Bool
    let conversion: Conversion
    let quality: Double
    let bookingToken: String
    let deepLink: String
    let trackingPixel: String?
    let p1: Int
    let p2: Int
    let p3: Int
    let transfers: [String]
    let typeFlights: [String]
    let popularity: Double?
    let hiddenCityTicketing: Bool
    let virtualInterlining: Bool
    let route: [Route]?
    
    enum CodingKeys: String, CodingKey {
        case id, dTime, dTimeUTC, aTime, aTimeUTC, nightsInDest, duration
        case flyDuration = "fly_duration"
        case flyFrom, cityFrom, cityCodeFrom, countryFrom, mapIdFrom, flyTo, cityTo, cityCodeTo, countryTo, distance, routes, airlines
        case mapIdTo = "mapIdto"
        case pnrCount = "pnr_count"
        case hasAirportChange = "has_airport_change"
        case technicalStops = "technical_stops"
        case price
        case bagsPrice = "bags_price"
        case baglimit, availability
        case facilitatedBookingAvailable = "facilitated_booking_available"
        case conversion, quality
        case bookingToken = "booking_token"
        case deepLink = "deep_link"
        case trackingPixel = "tracking_pixel"
        case p1, p2, p3, transfers
        case typeFlights = "type_flights"
        case popularity
        case hiddenCityTicketing = "hidden_city_ticketing"
        case virtualInterlining = "virtual_interlining"
        case route
    }
}
