//
//  Conversion.swift
//  kiwicomapp-entrytaskios
//
//  Created by Filip Laššo on 16/01/2021.
//  Copyright © 2021 Kiwi. All rights reserved.
//

public struct Conversion: Codable {
    
    let eur: Int
    
    enum CodingKeys: String, CodingKey {
        case eur = "EUR"
    }
}
