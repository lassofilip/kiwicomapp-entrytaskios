//
//  FlightsResponse.swift
//  kiwicomapp-entrytaskios
//
//  Created by Filip Laššo on 16/01/2021.
//  Copyright © 2021 Kiwi. All rights reserved.
//

public struct FlightsResponse: Codable {

    let searchId: String
    let data: [Flight]
    let connections: [String]
    let time: Int
    let currency: String
    let currencyRate: Int
    let fxRate: Int
    let refresh: [String]
    let del: Double?
    let refTasks: [String]
    let searchParams: SearchParams
    let allStopoverAirports: [String]
    let allAirlines: [String]
    let results: Int
    
    enum CodingKeys: String, CodingKey {
        case searchId = "search_id"
        case data, connections, time, currency
        case currencyRate = "currency_rate"
        case fxRate = "fx_rate"
        case refresh, del
        case refTasks = "ref_tasks"
        case searchParams = "search_params"
        case allStopoverAirports = "all_stopover_airports"
        case allAirlines = "all_airlines"
        case results = "_results"
    }
}
