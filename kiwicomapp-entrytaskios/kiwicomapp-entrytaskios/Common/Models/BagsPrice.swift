//
//  BagsPrice.swift
//  kiwicomapp-entrytaskios
//
//  Created by Filip Laššo on 16/01/2021.
//  Copyright © 2021 Kiwi. All rights reserved.
//

public struct BagsPrice: Codable {
    
    let first: Float?
    let second: Float?
    
    enum CodingKeys: String, CodingKey {
        case first = "1"
        case second = "2"
    }
}
