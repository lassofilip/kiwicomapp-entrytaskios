//
//  Route.swift
//  kiwicomapp-entrytaskios
//
//  Created by Filip Laššo on 16/01/2021.
//  Copyright © 2021 Kiwi. All rights reserved.
//

import Foundation

public struct Route: Codable {
    
    let fareBasis: String
    let fareCategory: String
    let fareClasses: String
    let price: Int
    let fareFamily: String
    let foundOn: String
    let lastSeen: Double
    let refreshTimestamp: Double
    let source: String
    let routeReturn: Int
    let bagsRecheckRequired: Bool
    let guarantee: Bool
    let id: String
    let combinationId: String
    let originalReturn: Int
    let aTime: Double
    let dTime: Double
    let aTimeUTC: Double
    let dTimeUTC: Double
    let mapIdfrom: String
    let mapIdto: String
    let cityTo: String
    let cityFrom: String
    let cityCodeFrom: String
    let cityCodeTo: String
    let flyTo: String
    let flyFrom: String
    let airline: String
    let operatingCarrier: String
    let equipment: String?
    let latFrom: Double
    let lngFrom: Double
    let latTo: Double
    let lngTo: Double
    let flightNo: Int
    let vehicleType: String
    let operatingFlightNo: String
    
    enum CodingKeys: String, CodingKey {
        case fareBasis = "fare_basis"
        case fareCategory = "fare_category"
        case fareClasses = "fare_classes"
        case price
        case fareFamily = "fare_family"
        case foundOn = "found_on"
        case lastSeen = "last_seen"
        case refreshTimestamp = "refresh_timestamp"
        case source
        case routeReturn = "return"
        case bagsRecheckRequired = "bags_recheck_required"
        case guarantee, id
        case combinationId = "combination_id"
        case originalReturn = "original_return"
        case aTime, dTime, aTimeUTC, dTimeUTC, mapIdfrom, mapIdto, cityTo, cityFrom, cityCodeFrom, cityCodeTo, flyTo, flyFrom, airline
        case operatingCarrier = "operating_carrier"
        case equipment, latFrom, lngFrom, latTo, lngTo
        case flightNo = "flight_no"
        case vehicleType = "vehicle_type"
        case operatingFlightNo = "operating_flight_no"
    }
}
