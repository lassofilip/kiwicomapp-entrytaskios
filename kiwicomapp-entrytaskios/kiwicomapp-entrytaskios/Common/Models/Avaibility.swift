//
//  Avaibility.swift
//  kiwicomapp-entrytaskios
//
//  Created by Filip Laššo on 16/01/2021.
//  Copyright © 2021 Kiwi. All rights reserved.
//

public struct Avaibility: Codable {
    
    let seats: Int?
}
