//
//  Baglimit.swift
//  kiwicomapp-entrytaskios
//
//  Created by Filip Laššo on 16/01/2021.
//  Copyright © 2021 Kiwi. All rights reserved.
//

public struct Baglimit: Codable {
    
    let handWidth: Int?
    let handHeight: Int?
    let handLength: Int?
    let handWeight: Int?
    let holdWidth: Int?
    let holdHeight: Int?
    let holdLength: Int?
    let holdDimensionsSum: Int?
    let holdWeight: Int?
    
    enum CodingKeys: String, CodingKey {
        case handWidth = "hand_width"
        case handHeight = "hand_height"
        case handLength = "hand_length"
        case handWeight = "hand_weight"
        case holdWidth = "hold_width"
        case holdHeight = "hold_height"
        case holdLength = "hold_length"
        case holdDimensionsSum = "hold_dimensions_sum"
        case holdWeight = "hold_weight"
    }
}
