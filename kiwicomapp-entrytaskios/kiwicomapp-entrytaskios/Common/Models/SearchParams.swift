//
//  SearchParams.swift
//  kiwicomapp-entrytaskios
//
//  Created by Filip Laššo on 16/01/2021.
//  Copyright © 2021 Kiwi. All rights reserved.
//

public struct SearchParams: Codable {
    
    let flyFromType: String
    let toType: String
    let seats: Seats
    
    enum CodingKeys: String, CodingKey {
        case flyFromType = "flyFrom_type"
        case toType = "to_type"
        case seats
    }
}
