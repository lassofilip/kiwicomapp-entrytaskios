//
//  AppDelegate.swift
//  kiwicomapp-entrytaskios
//
//  Created by Filip Laššo on 15/01/2021.
//  Copyright © 2021 Kiwi. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder,
    UIApplicationDelegate {

    var window: UIWindow?
    var appCoordinator: AppCoordinator!
    
    func application(
        _ application: UIApplication,
        didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
    ) -> Bool {
        window = UIWindow(frame: UIScreen.main.bounds)
        appCoordinator = AppCoordinator(in: window!)
        appCoordinator.start()
        return true
    }
}
